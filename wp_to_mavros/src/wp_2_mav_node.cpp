#include <ros/ros.h>
#include <ros/ros.h>
#include <mavros/mavros.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/CommandHome.h>
#include <mavros_msgs/CommandTOL.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf/LinearMath/Quaternion.h>
#include "traj_gen_node/wp_com.h"
#include <pthread.h>
#include <math.h>
#define PI 3.14159265
#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))

// p-thread calls
pthread_mutex_t topicBell;
pthread_cond_t newPose;

// Global Vars
mavros_msgs::State px4_state;
traj_gen_node::wp_com wp_val;
int offboard_mode_eq;
int manual_mode_eq;

void wpCallback(const traj_gen_node::wp_com& msg)
{
  wp_val = msg;
}

void stateCallback(const mavros_msgs::State& msg)
{
  px4_state = msg;
  offboard_mode_eq = strcmp("OFFBOARD",px4_state.mode.c_str());
  manual_mode_eq = strcmp("MANUAL",px4_state.mode.c_str());
  //ROS_INFO("Con: %d, Arm: %d, Gui: %d, Mode: %s, Truth: %d", px4_state.connected,px4_state.armed,px4_state.guided,px4_state.mode.c_str(),str_eq);
}


int main(int argc, char **argv)
{
  // Ros Init
  ros::init(argc, argv, "flynet_node");
  // Ros Handle
  ros::NodeHandle n;
  
  //======= ROS Publishers ================
  ros::Publisher mavros_pos_control_pub = n.advertise<geometry_msgs::PoseStamped>("pose_set_point",1000);  
  geometry_msgs::PoseStamped pos_sp;
  
  tf::Quaternion quat(0.,0.,0.,1.0);
  
  //======= ROS Subscribers ===============
  // Ros setup async spinners for subscibers
  ros::AsyncSpinner spinner(2);
  // Setup Subscribers:
  ros::Subscriber sub_state = n.subscribe("state", 1000, stateCallback);
  ros::Subscriber sub_wp = n.subscribe("wp_in", 1000, wpCallback);
  // Start the Spinner
  spinner.start();
  
  //==== ROS Clients/Services ==============
  // Mode Set Service
  ros::ServiceClient mavros_set_mode_client = n.serviceClient<mavros_msgs::SetMode>("/mavros/set_mode");
  mavros_msgs::SetMode set_mode;
  set_mode.request.custom_mode = "OFFBOARD";
  
  // Guided Enable Service
  ros::ServiceClient mavros_nav_guided_client = n.serviceClient<mavros_msgs::CommandBool>("/mavros/cmd/guided_enable");
  mavros_msgs::CommandBool nav_guided;
  nav_guided.request.value = true; 
  
  // Arming Service
  ros::ServiceClient mavros_arm_client = n.serviceClient<mavros_msgs::CommandBool>("/mavros/cmd/arming");
  mavros_msgs::CommandBool px4_arm;
  px4_arm.request.value = true; 
  
  
  //Set Home Service 
  ros::ServiceClient mavros_set_home_client = n.serviceClient<mavros_msgs::CommandHome>("/mavros/cmd/set_home");
  mavros_msgs::CommandHome px4_home;
  /*
  px4_home.request.current_gps = false;
  px4_home.request.latitude = 40.0021092;
  px4_home.request.longitude = -105.2632344;
  px4_home.request.altitude = 1650; //this needs fixing....
  */
  
  //Takeoff Service
  ros::ServiceClient mavros_takeoff_client = n.serviceClient<mavros_msgs::CommandTOL>("/mavros/cmd/takeoff");
  mavros_msgs::CommandTOL px4_to;
  /*
  px4_to.request.min_pitch = 0.15;//rads?
  px4_to.request.yaw = 0.0; // is this right? meh probs...
  px4_to.request.latitude = 40.0021092;
  px4_to.request.longitude = -105.2632344;
  px4_to.request.altitude = 1651.5; //this needs fixing....
  */
  
  //Landing Service 
  ros::ServiceClient mavros_land_client = n.serviceClient<mavros_msgs::CommandTOL>("/mavros/cmd/land");
  mavros_msgs::CommandTOL px4_la;
  /*
  px4_la.request.min_pitch = 0.15;//rads?
  px4_la.request.yaw = 0.0; // is this right? meh probs...
  px4_la.request.latitude = 40.0021092;
  px4_la.request.longitude = -105.2632344;
  px4_la.request.altitude = 1650; //this needs fixing....
  */
  // Publisher Loop
  ros::Rate loop_rate(50.0);
  int count = 0;
  mavros_nav_guided_client.call(nav_guided);
  mavros_set_mode_client.call(set_mode);
  mavros_arm_client.call(px4_arm);
  mavros_takeoff_client.call(px4_to);
  mavros_set_home_client.call(px4_home);
  //mavros_set_mode_client.call(set_mode);
  //mavros_arm_client.call(px4_arm);
  //ros::spinOnce();
  //

  while (ros::ok()){
  	++count;
    double yaw_sp = wp_val.yaw;
    double qz = sin(yaw_sp/2.0);
    double qw = cos(yaw_sp/2.0);
    // Write desired setpoint value to pos_sp 
	pos_sp.header.seq = count;
	pos_sp.header.stamp = ros::Time::now();
	pos_sp.header.frame_id = "fcu";
	pos_sp.pose.position.x = wp_val.x;
	pos_sp.pose.position.y = wp_val.y;
	pos_sp.pose.position.z = wp_val.z;
	// Write the Yaw SP in Quaternion
	pos_sp.pose.orientation.x = 0; 
	pos_sp.pose.orientation.y = 0; 
	pos_sp.pose.orientation.z = qz; 
	pos_sp.pose.orientation.w = qw; 
	
	mavros_pos_control_pub.publish(pos_sp);
  
  	// write a check to see if we're in offboard mode
  	
	if ((offboard_mode_eq!=0)&(manual_mode_eq!=0)&(!px4_state.armed)){
		mavros_set_mode_client.call(set_mode);
  		mavros_arm_client.call(px4_arm);
	}
	
	// Spin Once:
    ros::spinOnce();
	// Maintain loop rate
    loop_rate.sleep();
  }
  return 0;
}
