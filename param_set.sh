#!/bin/bash
 
WDIR=`pwd`
WORKSPACE=$WDIR/catkin_ws
source devel/setup.bash
rosrun mavros mavparam set ATT_MAG_DECL 8.60
rosrun mavros mavparam set CBRK_NO_VISION 0
rosrun mavros mavparam set MIS_YAWMODE 0   
rosrun mavros mavparam set ATT_EXT_HDG_M 1
