#!/bin/bash
WDIR=`pwd`
WORKSPACE=$WDIR/catkin_ws
source devel/setup.bash
roslaunch mavros px4.launch &
sleep 1m
rosrun ros_vrpn_client ros_vrpn_client __name:=Terminator _vrpn_server_ip:=192.168.20.100 &
sleep 5s
roslaunch vicon_to_mavros vic_2_mav.launch &
sleep 5s
roslaunch traj_gen_node traj_gen.launch &
sleep 5s
roslaunch wp_to_mavros wp_gen.launch &

