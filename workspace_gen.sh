#!/bin/bash
#
# Create workspace at current location and fetch source repositories
#
# License: according to LICENSE.md in the root directory of the PX4 Firmware repository
#set -e
 
WDIR=`pwd`
WORKSPACE=$WDIR/catkin_ws
 
# Setup workspace
mkdir -p $WORKSPACE/src
cd $WORKSPACE/src
catkin_init_workspace
cd $WORKSPACE
catkin_make
source devel/setup.bash
 
cd $WORKSPACE/src

# ROS VRPN_CLIENT SETUP
git clone https://github.com/damanfb/ros_vrpn_client.git
cd ros_vrpn_client
unzip vrpn_07_28.zip

# Compile workspace
cd $WORKSPACE
catkin_make
source devel/setup.bash

cd $WORKSPACE
cp -r ../vicon_to_mavros src/
cp -r ../traj_gen_node src/
cp -r ../wp_to_mavros src/

# Compile workspace
cd $WORKSPACE
catkin_make
source devel/setup.bash

